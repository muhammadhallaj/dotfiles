#!/bin/sh

_EDITOR=$(which vimx 2>/dev/null||which vim 2>/dev/null||which vi 2>/dev/null)

if [ ! -z "$_EDITOR" ]; then
  export EDITOR=$_EDITOR
fi

unset _EDITOR
