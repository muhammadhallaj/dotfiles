#!/bin/sh

_SWAYSOCK="${XDG_RUNTIME_DIR}/sway-ipc.$(id -u).$(pgrep -x sway).sock"

if [ -e $_SWAYSOCK ] && [ -S $_SWAYSOCK ]; then
  export SWAYSOCK=$_SWAYSOCK
fi

unset _SWAYSOCK
