# enable tmux

if which tmux &>/dev/null; then
  if [ -z "$TMUX" ]; then
    tmux new-session
  fi
fi
