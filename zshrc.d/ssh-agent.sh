# setting ssh-agent

gpg-agent --daemon &>/dev/null

if [ -e "${HOME}/.gnupg/S.gpg-agent.ssh" ]; then
  export SSH_AUTH_SOCK="${HOME}/.gnupg/S.gpg-agent.ssh"
elif [ -e "${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh" ]; then
  export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/gnupg/S.gpg-agent.ssh"
fi
